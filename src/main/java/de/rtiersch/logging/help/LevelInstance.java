/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.logging.help;


import de.rtiersch.logging.ELevel;
import de.rtiersch.logging.instance.IWriteToInstance;
import org.jetbrains.annotations.NotNull;

/**
 * Tuple class for readability purpose.
 * Which StreamInstance has which log level active?
 */
public class LevelInstance
{
    /**
     * Debug m_instance (like File, Syslog or Terminal.
     */
    private IWriteToInstance m_instance;
    /**
     * Log m_level for to use for the m_instance declared above.
     */
    private ELevel           m_level;

    /**
     * Initializes the tuple.
     *
     * @param p_level           Log m_level for to use for the m_instance
     *                          declared above.
     * @param p_writeToInstance Debug m_instance (like File, Syslog or
     *                          Terminal.
     */
    public LevelInstance(
        final ELevel p_level, final IWriteToInstance p_writeToInstance)
    {
        this.m_instance = p_writeToInstance;
        this.m_level = p_level;
    }

    /**
     * Returns the Streaming instance.
     *
     * @return Streamer/Out channel.
     */
    public IWriteToInstance getInstance()
    {
        return this.m_instance;
    }

    /**
     * Changes the output channel / stream.
     *
     * @param p_instance New stream.
     * @return self.
     */
    public @NotNull LevelInstance setInstance(
        @NotNull final IWriteToInstance p_instance)
    {
        this.m_instance = p_instance;
        return this;
    }

    /**
     * Returns the Logging level.
     *
     * @return Log Level.
     */
    public @NotNull ELevel getLevel()
    {
        return this.m_level;
    }

    /**
     * Sets the logging level for this stream.
     *
     * @param p_level New Level.
     * @return self.
     */
    public @NotNull LevelInstance setLevel(final ELevel p_level)
    {
        this.m_level = p_level;
        return this;
    }
}
