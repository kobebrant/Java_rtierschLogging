/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.logging.help;

import org.jetbrains.annotations.NotNull;

/**
 * Created by Raphael Tiersch on 30.08.2016.
 * @deprecated Will be deleted soon
 * TODO DELETE THIS!
 */
public class TraceId extends TraceLock {

    /**
     * Trace name.
     */
    private String m_trace;
    /**
     * Trace id.
     */
    private int    m_id;

    public TraceId(final @NotNull String p_trace, final int p_id) {
        this.m_trace = p_trace;
        this.m_id = p_id;
    }

    public TraceId() {
    }

    /**
     * Returns the trace name.
     * @return trace name.
     */
    public final String getTrace() {
        return this.m_trace;
    }

    /**
     * Sets the trace name.
     * @param p_trace new trace name.
     * @return self.
     */
    public final TraceId setTrace(final String p_trace) {
        this.m_trace = p_trace;
        return this;
    }

    /**
     * Retirns the trace id.
     * @return trace id.
     */
    public final int getId() {
        return this.m_id;
    }

    /**
     * Sets the trace id.
     * @param p_id new trace id.
     * @return self.
     */
    public final TraceId setId(final int p_id) {
        this.m_id = p_id;
        return this;
    }
}
