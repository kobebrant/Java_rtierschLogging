/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.logging.instance;

import org.jetbrains.annotations.NotNull;

/**
 * Created by Rafael on 02.10.2015.
 * TODO Refactor this class design. It is plain ugly.
 */
public class Terminal
    implements IWriteToInstance
{

    /**
     * Empty Constructor. This should not have any functionality if really
     * not needed. To start the "streaming" please use start().
     */
    public Terminal()
    {
    }

    @Override
    public boolean write(final String p_input)
    {
        System.out.println(p_input);
        return true;
    }

    @Override
    public @NotNull IWriteToInstance start()
    {
        return this;
    }

    @Override
    public @NotNull IWriteToInstance stop()
    {
        return this;
    }

    @Override
    public @NotNull IWriteToInstance setOutput(final Object p_output)
    {
        return this;
    }
}
