/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.logging;

import static de.rtiersch.logging.EDelimiter.LOG;

/**
 * Inner enum ELevel isOfChar the different log-levels. Higher m_value levels
 * isOfChar the previous ones.
 */
public enum ELevel
{
    /**
     * No logging at all.
     */
    NONE(0, "", EDelimiter.FORMAT_DEB_TEXT, EDelimiter.FORMAT_DEB_TEXT),
    /**
     * Only errors should be displayed.
     */
    ERROR(1, "ERROR", EDelimiter.FORMAT_ERR, EDelimiter.FORMAT_ERR_TEXT),
    /**
     * Errors and warnings should be displayed.
     */
    WARN(2, "WARN", EDelimiter.FORMAT_WAR, EDelimiter.FORMAT_WAR_TEXT),
    /**
     * Errors, warnings and information should be displayed.
     */
    INFO(3, "INFO", EDelimiter.FORMAT_INF, EDelimiter.FORMAT_INF_TEXT),
    /**
     * Errors, warnings, information and debug outputs should be displayed.
     */
    DEBUG(4, "DEBUG", EDelimiter.FORMAT_DEB, EDelimiter.FORMAT_DEB_TEXT);

    /**
     * hierarchic level description. Higher Number means higher level.
     */
    private final int    m_value;
    /**
     * String representation.
     */
    private final String m_name;

    /**
     * Format delimiters.
     */
    private final EDelimiter m_format;

    /**
     * Format delimiters for text.
     */
    private final EDelimiter m_formatText;

    /**
     * Initializes the Enum.
     *
     * @param p_level      int hierarchic number.
     * @param p_name       String reprenentation.
     * @param p_format     Format delimiters.
     * @param p_formatText Format delimiters for text.
     */
    ELevel(
        final int p_level,
        final String p_name,
        final EDelimiter p_format,
        final EDelimiter p_formatText)
    {
        this.m_value = p_level;
        this.m_name = p_name;
        this.m_format = p_format;
        this.m_formatText = p_formatText;
    }

    /**
     * Returns the hierarchic m_value of the enum.
     *
     * @return
     */
    public int getValue()
    {
        return this.m_value;
    }

    /**
     * String representation.
     *
     * @return String representation.
     */
    public final String getName()
    {
        return this.m_name;
    }

    /**
     * Format delimiters.
     *
     * @return Format delimiters.
     */
    public final EDelimiter getFormat()
    {
        return this.m_format;
    }

    /**
     * Format delimiters for text.
     *
     * @return Format delimiters for text.
     */
    public final EDelimiter getFormatText()
    {
        return this.m_formatText;
    }

    /**
     * @return
     */
    public final String getNameFmt()
    {
        return this.m_format.surround(LOG.surroundF(this.m_name));
    }
}
